const axios = require('axios')
const md5 = require('js-md5');

const API_VERSION = 'v1'
const TOKEN = '5e4xc4suqpdw8ap2slkv40zps'
const BASE_URL = 'https://sparkapi.com'

const axiosInstance = axios.create({
    baseURL: BASE_URL,
    timeout: 5000,
    headers: {
        //'Authorization': `Bearer ${TOKEN}`, 
        'Accept': 'application/json',
        'X-SparkApi-User-Agent': 'SparkAPIExamples'
    }
  });

const newContact = (contact) => {

    const data = {
        "DisplayName": contact.name,
        "PrimaryEmail": contact.email,
        "Tags": contact.tags
    }

    axiosInstance.post(`/${API_VERSION}/contacts`, data)
        .then(response => {
            console.log(response)
        })
        .catch(error => {
            console.log(error)
        })
}

const getSession = () => {

    const _serect = `9uq7q2tqgvh9ncovsexv6dcgz`;
    const _key = `a6wg7fq1uz5o4mzm04bjrpo1r`

    const data = {
        ApiKey: _key,
        ApiSig: md5(`${_serect}ApiKey${_key}`)
    }

    axiosInstance.post(`/${API_VERSION}/session`, data)
        .then(response => {
            console.log(response)
        })
        .catch(error => {
            console.log(error)
        })
}

getSession()
// const _contact = {
//     name: 'Wes Eldridge',
//     email: 'test@gmail.com',
//     tags: [
//         'Test'
//     ]
// }

// newContact(_contact)